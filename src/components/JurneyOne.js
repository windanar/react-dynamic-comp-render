import React from 'react';
import DynamicCompLoader from './DynamicCompLoader'

class JurneyOne extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            steps: ['ComponentOne', 'ComponentTwo', 'ComponentOne', 'ComponentTwo']
        }
    }

    render() {

        var components = this.state.steps.map((item, key) =>
            <DynamicCompLoader CompName={item} />
        );

        return (
            <div>
                {components}
            </div>
        );
    }
}

export default JurneyOne;
