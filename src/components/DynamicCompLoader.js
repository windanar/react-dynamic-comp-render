import React from 'react';
import CompOne from './CompOne';
import CompTwo from './CompTwo';



class DynamicCompLoader extends React.Component {
    constructor(props) {
        super(props);

        this.state = {

        }
    }

    components = {
        ComponentOne: CompOne,
        ComponentTwo: CompTwo
    };

    render() {
        const Child = this.components[this.props.CompName || 'ComponentOne'];
        return <Child />
    }
}

export default DynamicCompLoader;
